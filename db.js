const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'mspr',
  password: '',
  port: 5432,
})

module.exports = pool;