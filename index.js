const express = require('express');
const bodyParser = require('body-parser')
const app = express()
const multer = require('multer');
const upload = multer({ dest: 'upload/' });


//Routes
const user = require('./routes/user')

app.use(bodyParser.json())

//GET
app.get('/users', user.getAllUsers)

//POST
app.post('/users/add',upload.none(),user.insertUser)

//PUT
app.put('/users/update',upload.none(), user.updateUser)

//Serveur en écoute
app.listen(3000, ()=>{
  console.log("Serveur en écoute")
})

module.exports = app;
