const pool = require('./../db')

//Récupère tous les utilisateurs
const getAllUsers = (request, response) => {
    pool.query('SELECT * FROM "User"')
        .then(res => response.status(200).json(res.rows))
        .catch(err => response.status(500).json({error : err}))
}

//Insérer un utilisateur 
const insertUser = (request, response) => {
    const formData = request.body;
    var mail = formData.MAIL;
    var name = formData.NAME;
    var lastname = formData.LASTNAME;
    pool.query('INSERT INTO "User" Values (uuid_generate_v4(), $1,$2,$3,\'OUVERT\');',[mail,name,lastname])
        .then(res => response.status(200).json({ success: 'Utilisateur ajoutée !' }))
        .catch(err => response.status(500).json({ error: 'Impossible d\ajouter l\'utilisateur' + err }))
}

//Update user
const updateUser = (request, response) => {
    const formData = request.body;
    var statut = formData.STATUT;
    var id = formData.ID;
    pool.query('UPDATE "User" SET "Statut" = $1 WHERE "Id" = $2',[statut,id])
        .then(res => response.status(200).json({ success: 'Statut modifié' }))
        .catch(err => response.status(500).json({ error: 'Impossible de modifier le statut' + err }))
}


module.exports = {
    getAllUsers,
    insertUser,
    updateUser
}
